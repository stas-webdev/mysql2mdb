<?php

$templateFilePath = realpath('./template.mdb'); // ���� � ����� � �������� MDB

$resultFileName = 'export_' . date('d_m_Y-H_i_s') . '-' . rand() . '.mdb';    // ���������� ��� ��� ����� ��������
copy($templateFilePath, './' . $resultFileName);    // �������� ������
$resultFilePath = realpath($resultFileName);    // �������� ������ ���� � ����� ��������

$mysqlDb = new PDO('mysql:host=localhost;dbname=mdb;charset=utf8', 'root', ''); // ������� ���������� � MySQL (dataSource, dbuser, dbpass)
$mDb = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ={$resultFilePath}; Uid=; Pwd=;");    // ������� ����������� � �� � ����� ��������

/******************************************************************/

// �������� ��� ������ �� MySql-������� products
$sql = 'SELECT * FROM products';
$queryResult = $mysqlDb->query($sql);

// �������� ��� ������ � Mdb-������� products
foreach ($queryResult as $row) {
    $data = convertEncodingForMdb($row);    // ���� �� �������������� ������ � windows-1251, �� ���������� ���������
    $sql = "INSERT INTO products (`id`, `title`, `articul`, `category`) VALUES ('{$data['id']}', '{$data['title']}', '{$data['articul']}', '{$data['category']}')";
    $exportResult = $mDb->query($sql);
    if (!$exportResult) var_dump($mDb->errorInfo());
}

/******************************************************************/

// �������� ��� ������ �� MySql-������� categories
$sql = 'SELECT * FROM categories';
$queryResult = $mysqlDb->query($sql);

// �������� ��� ������ � Mdb-������� products
foreach ($queryResult as $row) {
    $data = convertEncodingForMdb($row);    // ���� �� �������������� ������ � windows-1251, �� ���������� ���������
    $sql = "INSERT INTO categories (`id`, `title`) VALUES ('{$data['id']}', '{$data['title']}')";
    $exportResult = $mDb->query($sql);
    if (!$exportResult) var_dump($mDb->errorInfo());
}

/******************************************************************/

// ������������ ��������� ������ � windows-1251
function convertEncodingForMdb ($data) {
    $result = array();
    foreach($data as $name => $value) {
        if (is_array($value)) {
            $result[$name] = convertEncodingForMdb($value);
        } else if (is_string($value)) {
            $result[$name] = mb_convert_encoding($value, 'windows-1251');
        } else {
            $result[$name] = $value;
        }
    }
    return $result;
}


